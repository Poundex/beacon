package net.poundex.beacon.server.card;

import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.client.Card;
import net.poundex.beacon.server.client.ClientCommand;
import net.poundex.beacon.server.client.ClientService;
import net.poundex.beacon.server.client.action.ShowCardAction;
import net.poundex.beacon.server.programme.ProgrammeService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CardService {
	
	private final ClientService clientService;
	private final ProgrammeService programmeService;
	
	private final Map<String, CardMetadata> cards = new ConcurrentHashMap<>();

	private record CardMetadata(String id, String programmeInstanceId, boolean last) {};

	@PostConstruct
	public void init() {
		clientService.getClientCommandStream()
				.filter(cc -> cc.getCommand().equals(ShowCardAction.COMMAND))
				.subscribe(this::onClientCommand);
		
		programmeService.getProgrammeStopNotifications()
				.subscribe(pi -> cards.values().stream()
						.filter(cm -> cm.programmeInstanceId().equals(pi.getId()))
						.forEach(cm -> dismissCard(cm.id())));
	}

	public void dismissCard(String id) {
		CardMetadata cardMetadata;
		if ((cardMetadata = cards.remove(id)) == null)
			return;

		clientService.sendCommand(ClientCommand.builder()
				.command("beacon.client.card.remove")
				.arg(Card.ARG_ID, id)
				.build());
		
		if(cardMetadata.last())
			programmeService.stop(cardMetadata.programmeInstanceId());
	}

	private void onClientCommand(ClientCommand cc) {
		Map<String, String> args = getArgs(cc);
		cards.put(args.get(Card.ARG_ID),
				new CardMetadata(
						args.get(Card.ARG_ID),
						args.get("beacon.client.card.programmeInstanceId"),
						Boolean.parseBoolean(args.get("beacon.client.card.last"))));
	}

	private static Map<String, String> getArgs(ClientCommand clientCommand) {
		return clientCommand.getArgs().stream()
				.collect(Collectors.toMap(
						ClientCommand.Arg::getKey,
						ClientCommand.Arg::getValue));
	}
}
