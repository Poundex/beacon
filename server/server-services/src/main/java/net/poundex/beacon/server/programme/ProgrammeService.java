package net.poundex.beacon.server.programme;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.server.Programme;
import net.poundex.beacon.server.ProgrammeInstance;
import net.poundex.beacon.server.actuator.ActuatorService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProgrammeService {

	private final ActuatorService actuatorService;

	private final Map<String, ProgrammeInstance> running = new ConcurrentHashMap<>();
	private final Sinks.Many<ProgrammeInstance> programmeStop = Sinks.many().multicast().directAllOrNothing();

	public ProgrammeInstance start(Programme programme) {
		String id = UUID.randomUUID().toString();

		ProgrammeInstance programmeInstance = new ProgrammeInstance(id, programme);
		running.put(id, programmeInstance);
		actuatorService.actuate(programmeInstance, programme.getStart(), () -> running.containsKey(id));
		log.info("Instance {} is newly started {}", id, programme.getCode());
		return programmeInstance;
	}
	
	public void stop(ProgrammeInstance programmeInstance) {
		if(running.remove(programmeInstance.getId()) == null)
			return;
		
		actuatorService.cancelAllFor(programmeInstance);
		programmeStop.tryEmitNext(programmeInstance);
	}

	public void stop(String programmeInstanceId) {
		if(programmeInstanceId != null)
			stop(running.get(programmeInstanceId));
	}
	
	public Flux<ProgrammeInstance> getProgrammeStopNotifications() {
		return programmeStop.asFlux();
	}
}
