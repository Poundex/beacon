package net.poundex.beacon.server.programme;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AutoProgrammeStarter implements CommandLineRunner {

	private final ProgrammeRepository programmeRepository;
	private final ProgrammeService programmeService;

	@Override
	public void run(String... args) throws Exception {
		programmeRepository.findAllByAuto(true).subscribe(programmeService::start);
	}
}
