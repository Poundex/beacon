package net.poundex.beacon.server.actuator;

import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.ProgrammeInstance;
import net.poundex.beacon.server.client.ClientCommand;
import net.poundex.beacon.server.client.ClientService;
import net.poundex.beacon.server.client.action.ClientAction;
import net.poundex.beacon.server.task.AbstractTask;
import net.poundex.beacon.server.task.AlarmTask;
import net.poundex.beacon.server.task.DelayedTask;
import net.poundex.beacon.server.task.RepeatingTask;
import net.poundex.beacon.server.util.DateTimeUtil;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

@Service
@RequiredArgsConstructor
public class ActuatorService {

	private final TaskScheduler taskScheduler;
	private final ClientService clientService;
	
	private final MultiValuedMap<ProgrammeInstance, ScheduledFuture<?>> scheduledTasks = new HashSetValuedHashMap<>();
	
	public void actuate(ProgrammeInstance r, List<Actuator> actuatorList, InstanceChecker instanceChecker) {
		actuate(actuatorContext(r, instanceChecker), actuatorList);
	}

	private void actuate(ActuatorContext actuatorContext, List<Actuator> actuatorList) {
		actuatorList.forEach(a -> a.actuate(actuatorContext));
	}

	private ActuatorContext actuatorContext(ProgrammeInstance programmeInstance, InstanceChecker instanceChecker) {
		return new ActuatorContext() {
			public void scheduleDelayedTask(DelayedTask delayedTask) {
				scheduledTasks.put(programmeInstance,
						taskScheduler.schedule(runTask(delayedTask), Instant.now().plus(delayedTask.getDelay())));
			}

			public void scheduleAlarmTask(AlarmTask alarmTask) {
				scheduledTasks.put(programmeInstance, 
						taskScheduler.schedule(runTask(alarmTask), DateTimeUtil.next(alarmTask.getTime())));
			}

			public void scheduleRepeatingTask(RepeatingTask repeatingTask) {
				scheduledTasks.put(programmeInstance,
						taskScheduler.scheduleAtFixedRate(runTask(repeatingTask), repeatingTask.getInterval()));
			}

			private Runnable runTask(AbstractTask task) {
				return instanceChecker.ifRunningThen(() -> actuate(this, task.getActuatorList()));
			}

			@Override
			public void sendClientCommand(ClientAction clientAction) {
				ClientCommand.Builder clientCommandBuilder = ClientCommand.builder();
				clientAction.toCommand(clientCommandBuilder, this);
				clientCommandBuilder.arg("beacon.client.card.programmeInstanceId", programmeInstance.getId());
				clientService.sendCommand(clientCommandBuilder.build());
			}

			@Override
			public ProgrammeInstance getProgrammeInstance() {
				return programmeInstance;
			}
		};
	}

	public void cancelAllFor(ProgrammeInstance programmeInstance) {
		scheduledTasks.get(programmeInstance).forEach(sf -> sf.cancel(false));
	}
	
	public interface InstanceChecker {
		
		boolean isInstanceRunning();
		
		default Runnable ifRunningThen(Runnable runnable) {
			return () -> {
				if(isInstanceRunning())
					runnable.run();
			};
		}
	}
}
