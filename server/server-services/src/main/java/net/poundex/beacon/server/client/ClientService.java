package net.poundex.beacon.server.client;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

@Service
@RequiredArgsConstructor
public class ClientService {
	private final Sinks.Many<ClientCommand> commandSink = Sinks.many().multicast().directAllOrNothing();

	public void sendCommand(ClientCommand command) {
		commandSink.tryEmitNext(command);
	}
	
	public Flux<ClientCommand> getClientCommandStream() {
		return commandSink.asFlux();
	}
}
