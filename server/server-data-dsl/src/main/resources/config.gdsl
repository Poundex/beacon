import com.intellij.openapi.diagnostic.Logger
import com.intellij.psi.PsiClass
import org.jetbrains.plugins.groovy.dsl.CustomMembersGenerator
import org.jetbrains.plugins.groovy.dsl.GdslScriptBase
import org.jetbrains.plugins.groovy.dsl.toplevel.Context

class BeaconConfigDslGdsl {
	private static final CONFIG_FILE_NAME = "config.groovy"

	private static final Logger log = Logger.getInstance("beaconGdsl")

	private final GdslScriptBase base
	private final Context configFileContext
	private final Context programmeContext

	BeaconConfigDslGdsl(GdslScriptBase base) {
		this.base = base
		configFileContext = base.context(scope: base.scriptScope(name: CONFIG_FILE_NAME)) as Context
		programmeContext = base.context(scope: base.closureScope(isArg: true)) as Context
	}

	void contribute() {
//		try {
		doContribute()
//		}
//		catch (Exception ex) {
//			log.error(ex)
//			throw ex
//		}
	}

	private void doContribute() {
		contributeTo(configFileContext) {
			method name: "programme", params: [s: "groovy.lang.Closure"]
		}

		contributeTo(programmeContext, "programme") {
			injectMethods(delegate, "net.poundex.beacon.server.config.dsl.ProgrammeConfig")
		}

		contributeTo(programmeContext, ["start"]) {
			injectAll(delegate)
		}

		contributeTo(programmeContext, ["after", "at"]) {
			injectMethods(delegate, "net.poundex.beacon.server.config.dsl.ClientControl")
		}

		contributeTo(programmeContext, ["addCard"]) {
			injectMethods(delegate, "net.poundex.beacon.server.config.dsl.ClientControl.CardConfig")
		}
	}

	private void contributeTo(Context context, @DelegatesTo(CustomMembersGenerator) Closure<?> closure) {
		base.contributor([context], closure)
	}

	private void contributeTo(Context context, String inCall, @DelegatesTo(CustomMembersGenerator) Closure<?> closure) {
		base.contributor([context]) {
			if (enclosingCall(inCall))
				closure.rehydrate(delegate, owner, this).call()
		}
	}

	private void contributeTo(Context context, List<String> inCall, @DelegatesTo(CustomMembersGenerator) Closure<?> closure) {
		base.contributor([context]) {
			if (inCall.any { test -> enclosingCall(test) })
				closure.rehydrate(delegate, owner, this).call()
		}
	}

	private static void injectMethods(CustomMembersGenerator builder, String fullClassName) {
		PsiClass psiClass = builder.findClass(fullClassName)
		psiClass?.methods?.each {
			if (it.containingClass.name == 'Object')
				return

			Map<Object, Object> params = [:]
			it.parameterList.parameters.each {
				params[it.name] = it.type?.canonicalText
			}
			builder.method name: it.name, type: it.returnType?.canonicalText, params: params
		}
	}

	private static void injectAll(CustomMembersGenerator delegate) {
		injectMethods(delegate, "net.poundex.beacon.server.config.dsl.ScheduleControl")
		injectMethods(delegate, "net.poundex.beacon.server.config.dsl.ClientControl")
		injectMethods(delegate, "net.poundex.beacon.server.config.dsl.DebugControl")
	}
}

try {
	new BeaconConfigDslGdsl(this as GdslScriptBase).contribute()
}
catch (Exception ex) {
	Logger.getInstance("beaconGdsl").error("In GDSL", ex)
}