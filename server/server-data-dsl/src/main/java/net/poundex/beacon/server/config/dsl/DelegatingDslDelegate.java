package net.poundex.beacon.server.config.dsl;

import groovy.lang.Closure;
import groovy.lang.MissingMethodException;
import net.poundex.beacon.server.Debug;
import net.poundex.beacon.server.Schedule;
import net.poundex.beacon.server.actuator.Actuator;
import net.poundex.beacon.server.client.action.ClientActions;
import org.codehaus.groovy.runtime.DefaultGroovyMethods;

import java.util.List;

public abstract class DelegatingDslDelegate {

	protected List<Actuator> configure(Closure<?> closure) {
		ClientActions.ClientActionsBuilder clientActionsBuilder = ClientActions.builder();
		Schedule.ScheduleBuilder scheduleBuilder = Schedule.builder();
		Debug.DebugBuilder debugBuilder = Debug.builder();

		List<Object> delegates = List.of(
				new ScheduleDslDelegate(scheduleBuilder),
				new ClientControlDslDelegate(clientActionsBuilder),
				new DebugDslDelegate(debugBuilder));

		Object delegate = new Object() {
			public Object methodMissing(String name, Object args) {
				return DefaultGroovyMethods.invokeMethod(
						delegates.stream()
								.filter(d -> ! DefaultGroovyMethods.respondsTo(d, name).isEmpty())
								.findAny()
								.orElseThrow(() -> new MissingMethodException(name, null, (Object[]) args)), name, args);
			}
		};
		closure.rehydrate(delegate, delegate, delegate).call();
		return List.of(clientActionsBuilder.build(), scheduleBuilder.build(), debugBuilder.build());
	}
}
