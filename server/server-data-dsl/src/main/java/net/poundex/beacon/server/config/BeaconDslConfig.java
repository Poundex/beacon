package net.poundex.beacon.server.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.file.Path;


@ConfigurationProperties(prefix = "beacon.dsl")
@Data
@NoArgsConstructor
public class BeaconDslConfig {
	private Path configLocation;
}
