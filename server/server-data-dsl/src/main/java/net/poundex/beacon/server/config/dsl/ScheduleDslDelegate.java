package net.poundex.beacon.server.config.dsl;

import groovy.lang.Closure;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.Schedule;
import net.poundex.beacon.server.task.AlarmTask;
import net.poundex.beacon.server.task.DelayedTask;
import net.poundex.beacon.server.task.RepeatingTask;

import java.time.Duration;
import java.time.LocalTime;

@RequiredArgsConstructor
class ScheduleDslDelegate extends DelegatingDslDelegate {

	private final Schedule.ScheduleBuilder scheduleBuilder;

	public void after(Duration delay, Closure<Void> fn) {
		scheduleBuilder.delayedTask(new DelayedTask(delay, configure(fn)));
	}

	public void at(LocalTime time, Closure<Void> fn) {
		scheduleBuilder.alarmTask(new AlarmTask(time, configure(fn)));
	}

	public void every(Duration time, Closure<Void> fn) {
		scheduleBuilder.repeatingTask(new RepeatingTask(time, configure(fn)));
	}

}
