package net.poundex.beacon.server.config.dsl;

import groovy.lang.Closure;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.Programme;

@RequiredArgsConstructor
public class ProgrammeDslDelegate extends DelegatingDslDelegate {
	private final Programme.ProgrammeBuilder builder;

	public void setName(String name) {
		builder.name(name);
	}

	public void setCode(String code) {
		builder.code(code);
	}

	public void setDescription(String description) {
		builder.description(description);
	}

	public void setAuto(boolean auto) {
		builder.auto(auto);
	}

	public void start(Closure<?> closure) {
		builder.start(configure(closure));
	}
}
