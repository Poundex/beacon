package net.poundex.beacon.server.config.dsl;

import groovy.lang.Closure;
import groovy.lang.Script;
import net.poundex.beacon.server.Programme;

import java.util.ArrayList;
import java.util.List;

public abstract class ConfigDsl extends Script {

	private final List<Programme> seen = new ArrayList<>();

	public void programme(Closure<?> closure) {
		Programme.ProgrammeBuilder programmeBuilder = Programme.builder();
		ProgrammeDslDelegate programmeDslDelegate = new ProgrammeDslDelegate(programmeBuilder);
		closure.rehydrate(programmeDslDelegate, programmeDslDelegate, programmeDslDelegate).call();
		seen.add(programmeBuilder.build());
	}

	public List<Programme> getConfig() {
		return seen;
	}
}
