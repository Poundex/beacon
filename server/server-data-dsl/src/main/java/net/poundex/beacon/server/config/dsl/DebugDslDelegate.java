package net.poundex.beacon.server.config.dsl;

import groovy.lang.Closure;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.server.Debug;
import net.poundex.beacon.server.debug.LogAction;

@RequiredArgsConstructor
@Slf4j
class DebugDslDelegate {

	private final Debug.DebugBuilder debugBuilder;

	public void log(Closure<?> fn) {
		DebugDelegate delegate = new DebugDelegate();
		fn.rehydrate(delegate, delegate, delegate).call();
	}

	private class DebugDelegate {
		public void setText(String text) {
			debugBuilder.log(new LogAction(text));
		}
	}
}
