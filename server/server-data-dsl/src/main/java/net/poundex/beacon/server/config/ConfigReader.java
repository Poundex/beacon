package net.poundex.beacon.server.config;

import groovy.lang.GroovyShell;
import groovy.lang.Script;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.Programme;
import net.poundex.beacon.server.config.dsl.ConfigDsl;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConfigReader {

	private final BeaconDslConfig dslConfig;
	
	private List<Programme> cached = null;

	public List<Programme> getConfig() {
		if (cached == null)
			cached = doGetConfig();

		return cached;
	}

	private List<Programme> doGetConfig() {
		CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
		compilerConfiguration.setScriptBaseClass(ConfigDsl.class.getName());

		GroovyShell groovyShell = new GroovyShell(this.getClass().getClassLoader(), compilerConfiguration);

		return Try.withResources(() -> Files.newBufferedReader(dslConfig.getConfigLocation()))
				.of(groovyShell::parse)
				.map(s -> (ConfigDsl) s)
				.peek(Script::run)
				.map(ConfigDsl::getConfig)
				.get();
	}
}
