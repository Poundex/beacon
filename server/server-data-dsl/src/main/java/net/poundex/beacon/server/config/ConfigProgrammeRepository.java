package net.poundex.beacon.server.config;

import io.vavr.collection.Set;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.keypad.Keypad;
import net.poundex.beacon.server.Programme;
import net.poundex.beacon.server.programme.ProgrammeRepository;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class ConfigProgrammeRepository implements ProgrammeRepository {

	private final ConfigReader configReader;
	
	private NavigableMap<String, Programme> programmesByNumericCode;

	@Override
	public Mono<Programme> findById(String s) {
		return Mono.justOrEmpty(
				configReader.getConfig().stream()
						.filter(p -> p.getCode().equals(s))
						.findFirst());
	}

	@Override
	public Mono<Programme> findById(Publisher<String> id) {
		return Mono.from(id).flatMap(this::findById);
	}

	@Override
	public Mono<Boolean> existsById(String s) {
		return findById(s).map(ignored -> true).defaultIfEmpty(false);
	}

	@Override
	public Mono<Boolean> existsById(Publisher<String> id) {
		return Mono.from(id).flatMap(this::existsById);
	}

	@Override
	public Flux<Programme> findAll() {
		return Flux.fromIterable(configReader.getConfig());
	}

	@Override
	public Flux<Programme> findAllById(Iterable<String> strings) {
		Set<String> strings2 = Stream.ofAll(strings).toSet();
		return Flux.fromStream(configReader.getConfig().stream()
				.filter(s -> strings2.contains(s.getCode())));
	}

	@Override
	public Flux<Programme> findAllById(Publisher<String> idStream) {
		return Flux.from(idStream).flatMap(this::findById);
	}

	@Override
	public Flux<Programme> findAllByAuto(boolean auto) {
		return Flux.fromStream(configReader.getConfig().stream()
				.filter(p -> p.isAuto() == auto));
	}

	@Override
	public Flux<Programme> findAllByNumericCodeStartingWith(String q) {
		return Flux.fromStream(getProgrammesByNumericCode()
				.subMap(q, q + Character.MAX_VALUE).values().stream());
	}
	
	private NavigableMap<String, Programme> getProgrammesByNumericCode() {
		if(programmesByNumericCode == null)
			programmesByNumericCode = doGetProgrammesByNumericCode();
		
		return programmesByNumericCode;
	}

	private NavigableMap<String, Programme> doGetProgrammesByNumericCode() {
		record ProgrammeByCode(String code, Programme programme) {}
		
		NavigableMap<String, Programme> r = new TreeMap<>();
		configReader.getConfig().stream()
				.map(p -> new ProgrammeByCode(p.getCode(), p))
				.forEach(pc -> r.put(Keypad.INSTANCE.toNumeric(pc.code()), pc.programme()));
		return r;
	}

	@Override
	public Mono<Long> count() {
		return Mono.just((long) configReader.getConfig().size());
	}

	@Override
	public <S extends Programme> Mono<S> save(S entity) {
		return null;
	}

	@Override
	public <S extends Programme> Flux<S> saveAll(Iterable<S> entities) {
		return null;
	}

	@Override
	public <S extends Programme> Flux<S> saveAll(Publisher<S> entityStream) {
		return null;
	}

	@Override
	public Mono<Void> deleteById(String s) {
		return null;
	}

	@Override
	public Mono<Void> deleteById(Publisher<String> id) {
		return null;
	}

	@Override
	public Mono<Void> delete(Programme entity) {
		return null;
	}

	@Override
	public Mono<Void> deleteAllById(Iterable<? extends String> strings) {
		return null;
	}

	@Override
	public Mono<Void> deleteAll(Iterable<? extends Programme> entities) {
		return null;
	}

	@Override
	public Mono<Void> deleteAll(Publisher<? extends Programme> entityStream) {
		return null;
	}

	@Override
	public Mono<Void> deleteAll() {
		return null;
	}

}
