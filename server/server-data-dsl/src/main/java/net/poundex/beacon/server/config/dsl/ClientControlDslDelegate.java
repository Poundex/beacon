package net.poundex.beacon.server.config.dsl;

import groovy.lang.Closure;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.client.Card;
import net.poundex.beacon.server.client.action.ClientActions;
import net.poundex.beacon.server.client.action.PlaySoundAction;
import net.poundex.beacon.server.client.action.ShowCardAction;

@RequiredArgsConstructor
class ClientControlDslDelegate {

	private final ClientActions.ClientActionsBuilder clientActionsBuilder;

	public void playSound(String name) {
		clientActionsBuilder.action(new PlaySoundAction(name));
	}

	public void addCard(Closure<?> fn) {
		Card.CardBuilder cardBuilder = Card.builder();
		CardDslDelegate cardDslDelegate = new CardDslDelegate(cardBuilder);
		fn.rehydrate(cardDslDelegate, cardDslDelegate, cardDslDelegate).call();
		clientActionsBuilder.action(new ShowCardAction(cardBuilder.build()));
	}

}
