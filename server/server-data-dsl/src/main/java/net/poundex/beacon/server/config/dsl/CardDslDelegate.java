package net.poundex.beacon.server.config.dsl;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.client.Card;

@Data
@RequiredArgsConstructor
public class CardDslDelegate {

	private final Card.CardBuilder cardBuilder;

	public void setText(String text) {
		cardBuilder.text(text);
	}
	
	public void setId(String id) {
		cardBuilder.id(id);
	}
	
	public void setLast(boolean last) {
		cardBuilder.last(last);
	}
}
