package net.poundex.beacon.server.task;


import lombok.Getter;
import lombok.ToString;
import net.poundex.beacon.server.actuator.Actuator;

import java.time.Duration;
import java.util.List;

@Getter
@ToString(callSuper = true)
public class RepeatingTask extends AbstractTask {
	private final Duration interval;

	public RepeatingTask(Duration interval, List<Actuator> actuatorList) {
		super(actuatorList);
		this.interval = interval;
	}
}
