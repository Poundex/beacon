package net.poundex.beacon.server;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.server.actuator.Actuator;
import net.poundex.beacon.server.actuator.ActuatorContext;
import net.poundex.beacon.server.debug.LogAction;

import java.util.List;

@Builder
@RequiredArgsConstructor
@Data
@Slf4j
public class Debug implements Actuator {

	@Singular("log")
	private final List<LogAction> logActions;

	@Override
	public void actuate(ActuatorContext context) {
		logActions.forEach(la -> log.info(la.getText()));
	}
}   
