package net.poundex.beacon.server.actuator;

public interface Actuator {
	void actuate(ActuatorContext context);
}
