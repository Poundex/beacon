package net.poundex.beacon.server.task;


import lombok.Getter;
import lombok.ToString;
import net.poundex.beacon.server.actuator.Actuator;

import java.time.Duration;
import java.util.List;

@Getter
@ToString(callSuper = true)
public class DelayedTask extends AbstractTask {
	private final Duration delay;

	public DelayedTask(Duration delay, List<Actuator> actuatorList) {
		super(actuatorList);
		this.delay = delay;
	}
}
