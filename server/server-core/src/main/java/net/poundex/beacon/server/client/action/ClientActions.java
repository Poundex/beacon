package net.poundex.beacon.server.client.action;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import net.poundex.beacon.server.actuator.Actuator;
import net.poundex.beacon.server.actuator.ActuatorContext;

import java.util.List;

@RequiredArgsConstructor
@Builder
@Data
public class ClientActions implements Actuator {

	@Singular("action")
	private final List<ClientAction> clientActionList;

	@Override
	public void actuate(ActuatorContext context) {
		clientActionList.forEach(context::sendClientCommand);
	}
}
