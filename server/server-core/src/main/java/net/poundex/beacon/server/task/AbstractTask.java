package net.poundex.beacon.server.task;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.poundex.beacon.server.actuator.Actuator;

import java.util.List;

@RequiredArgsConstructor
@Getter
@ToString
public class AbstractTask implements Task {
	private final List<Actuator> actuatorList;
}
