package net.poundex.beacon.server.client;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
@Builder
@Data
public class Card {
	public static final String UNSPECIFIED = "_UNSPECIFIED";
	
	public final static String ARG_ID = "beacon.client.card.id";
	public final static String ARG_TEXT = "beacon.client.card.text";
	public final static String ARG_LAST = "beacon.client.card.last";

	@Builder.Default
	private final String id = UNSPECIFIED;
	private final String text;
	@Builder.Default
	private final boolean last = false;

	public void configureCommand(ClientCommand.Builder command, String programmeInstanceId) {
		command.arg(ARG_ID, String.format("%s/%s", programmeInstanceId, getId()));
		command.arg(ARG_TEXT, text);
		command.arg(ARG_LAST, Boolean.toString(last));
	}
	
	private String getId() {
		if(id.equals(UNSPECIFIED))
			return UUID.randomUUID().toString();
		
		return id;
	}
}
