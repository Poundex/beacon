package net.poundex.beacon.server.debug;

import lombok.Data;

@Data
public class LogAction {
	private final String text;
}
