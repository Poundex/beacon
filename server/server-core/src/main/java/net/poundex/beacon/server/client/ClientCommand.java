package net.poundex.beacon.server.client;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Data
public class ClientCommand {
	private final String command;
	private final List<Arg> args;

	private ClientCommand(String command, List<Arg> args) {
		this.command = command;
		this.args = args;
	}

	public static Builder builder() {
		return new Builder();
	}
	
	public static class Builder {
		private String command;
		private final List<Arg> args = new LinkedList<>();
		
		public Builder command(String command) {
			this.command = command;
			return this;
		}
		
		public Builder arg(String key, String value) {
			this.args.add(new Arg(key, value));
			return this;
		}
		
		public ClientCommand build() {
			Objects.requireNonNull(this.command, "command");
			return new ClientCommand(command, List.copyOf(args));
		}
	}
	
	@RequiredArgsConstructor
	@Data
	public static class Arg {
		private final String key;
		private final String value;
	}
}
