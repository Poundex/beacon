package net.poundex.beacon.server.task;


import lombok.Getter;
import lombok.ToString;
import net.poundex.beacon.server.actuator.Actuator;

import java.time.LocalTime;
import java.util.List;

@Getter
@ToString(callSuper = true)
public class AlarmTask extends AbstractTask {
	private final LocalTime time;

	public AlarmTask(LocalTime time, List<Actuator> actuatorList) {
		super(actuatorList);
		this.time = time;
	}
}
