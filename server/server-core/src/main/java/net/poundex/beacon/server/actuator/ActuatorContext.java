package net.poundex.beacon.server.actuator;

import net.poundex.beacon.server.ProgrammeInstance;
import net.poundex.beacon.server.client.action.ClientAction;
import net.poundex.beacon.server.task.AlarmTask;
import net.poundex.beacon.server.task.DelayedTask;
import net.poundex.beacon.server.task.RepeatingTask;

public interface ActuatorContext {
	void scheduleDelayedTask(DelayedTask delayedTask);

	void scheduleAlarmTask(AlarmTask alarmTask);

	void scheduleRepeatingTask(RepeatingTask repeatingTask);

	void sendClientCommand(ClientAction clientAction);
	
	ProgrammeInstance getProgrammeInstance();
}
