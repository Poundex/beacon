package net.poundex.beacon.server;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.Instant;

@Data
@RequiredArgsConstructor
public class ProgrammeInstance {
	private final String id;
	private final Programme programme;
	private final Instant started = Instant.now();
}
