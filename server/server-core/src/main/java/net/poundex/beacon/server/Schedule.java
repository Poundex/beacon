package net.poundex.beacon.server;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import net.poundex.beacon.server.actuator.Actuator;
import net.poundex.beacon.server.actuator.ActuatorContext;
import net.poundex.beacon.server.task.AlarmTask;
import net.poundex.beacon.server.task.DelayedTask;
import net.poundex.beacon.server.task.RepeatingTask;

import java.util.List;

@Builder
@RequiredArgsConstructor
@Data
public class Schedule implements Actuator {

	@Singular("delayedTask")
	private final List<DelayedTask> delayedTasks;

	@Singular("alarmTask")
	private final List<AlarmTask> alarmTasks;

	@Singular("repeatingTask")
	private final List<RepeatingTask> repeatingTasks;

	@Override
	public void actuate(ActuatorContext context) {
		delayedTasks.forEach(context::scheduleDelayedTask);
		alarmTasks.forEach(context::scheduleAlarmTask);
		repeatingTasks.forEach(context::scheduleRepeatingTask);
	}
}
