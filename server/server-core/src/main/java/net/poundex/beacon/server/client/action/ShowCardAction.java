package net.poundex.beacon.server.client.action;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.actuator.ActuatorContext;
import net.poundex.beacon.server.client.Card;
import net.poundex.beacon.server.client.ClientCommand;

@RequiredArgsConstructor
@Data
public class ShowCardAction implements ClientAction {
	public final static String COMMAND = "beacon.client.card.show";
	
	private final Card card;
	
	@Override
	public void toCommand(ClientCommand.Builder clientCommandBuilder, ActuatorContext actuatorContext) {
		clientCommandBuilder.command(COMMAND);
		card.configureCommand(clientCommandBuilder, actuatorContext.getProgrammeInstance().getId());
	}
}
