package net.poundex.beacon.server.client.action;

import net.poundex.beacon.server.actuator.ActuatorContext;
import net.poundex.beacon.server.client.ClientCommand;

public interface ClientAction {
	void toCommand(ClientCommand.Builder clientCommandBuilder, ActuatorContext actuatorContext);
}
