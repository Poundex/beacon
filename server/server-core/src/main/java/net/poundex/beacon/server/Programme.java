package net.poundex.beacon.server;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.actuator.Actuator;

import java.util.List;

@Data
@RequiredArgsConstructor
@Builder
public class Programme {
	private final String code;
	private final String name;
	private final String description;
	private final boolean auto;
	private final List<Actuator> start;
}
