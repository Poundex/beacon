package net.poundex.beacon.server.util;

import java.time.*;

public class DateTimeUtil {

	public static Instant next(LocalTime time) {
		LocalDateTime dt = LocalDate.now().atTime(time);

		if (dt.isBefore(LocalDateTime.now()))
			dt = dt.plusDays(1);

		return dt.toInstant(ZoneOffset.UTC);
	}
}
