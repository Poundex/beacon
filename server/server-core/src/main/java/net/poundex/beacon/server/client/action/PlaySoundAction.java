package net.poundex.beacon.server.client.action;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.server.actuator.ActuatorContext;
import net.poundex.beacon.server.client.ClientCommand;

@RequiredArgsConstructor
@Data
public class PlaySoundAction implements ClientAction {
	private final static String COMMAND = "beacon.client.sound.play";
	private final static String ARG_NAME = "beacon.client.sound.name";

	private final String name;

	@Override
	public void toCommand(ClientCommand.Builder clientCommandBuilder, ActuatorContext actuatorContext) {
		clientCommandBuilder
				.command(COMMAND)
				.arg(ARG_NAME, name)
				.build();
	}
}
