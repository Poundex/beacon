package net.poundex.beacon.server.programme;

import net.poundex.beacon.server.Programme;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ProgrammeRepository extends ReactiveCrudRepository<Programme, String> {
	Flux<Programme> findAllByAuto(boolean auto);
	Flux<Programme> findAllByNumericCodeStartingWith(String q);
}
