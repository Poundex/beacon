package net.poundex.beacon.server.card;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

import java.util.Map;

@Controller
@RequiredArgsConstructor
@Slf4j
public class CardController {
	
	private final CardService cardService; 
	
	@MutationMapping
	public Mono<Map<String, Object>> dismissCard(@Argument String id) {
		cardService.dismissCard(id);
		return Mono.just(Map.of("id", id));
	}
}
