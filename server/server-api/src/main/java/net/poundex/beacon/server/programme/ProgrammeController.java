package net.poundex.beacon.server.programme;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.server.Programme;
import net.poundex.beacon.server.ProgrammeInstance;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@RequiredArgsConstructor
@Slf4j
public class ProgrammeController {
	
	private final ProgrammeRepository programmeRepository;
	private final ProgrammeService programmeService;
	
	@QueryMapping
	public Flux<Programme> programmes() {
		return programmeRepository.findAll();
	}
	
	@QueryMapping
	public Flux<Programme> programmeSearch(@Argument String q) {
		return programmeRepository.findAllByNumericCodeStartingWith(q);
	}
	
	@MutationMapping
	public Mono<ProgrammeInstance> startProgramme(@Argument String code) {
		return programmeRepository.findById(code).map(programmeService::start);
	}
}
