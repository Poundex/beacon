package net.poundex.beacon.server.client;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.graphql.data.method.annotation.SubscriptionMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;

@Controller
@RequiredArgsConstructor
@Slf4j
public class ClientController {
	
	private final ClientService clientService;
	
	@SubscriptionMapping
	public Flux<ClientCommand> clientCommands() {
		return clientService.getClientCommandStream();
	}
}
