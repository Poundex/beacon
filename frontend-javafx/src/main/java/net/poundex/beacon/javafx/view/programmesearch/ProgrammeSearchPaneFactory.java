package net.poundex.beacon.javafx.view.programmesearch;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.javafx.client.BeaconClient;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ProgrammeSearchPaneFactory {

	private final BeaconClient beaconClient;

	public ProgrammeSearchPane getProgrammeSearchPane(Runnable dialogCloser) {
		ProgrammeSearchPane r = new ProgrammeSearchPane(beaconClient, dialogCloser);
		r.init();
		return r;
	}
}
