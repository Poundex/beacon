package net.poundex.beacon.javafx.view;

import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import lombok.RequiredArgsConstructor;
import net.poundex.beacon.javafx.util.JMetroUtil;
import net.poundex.beacon.javafx.view.programmesearch.ProgrammeSearchDialog;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class SidePanel extends VBox {
	
	private final ObjectProvider<ProgrammeSearchDialog> programmeSearchDialogObjectProvider;
	
	@PostConstruct
	public void init() {
		JMetroUtil.INSTANCE.applyTheme(this).background(this);
		Button b = new Button("+");
		b.setFocusTraversable(false);
		b.setStyle("-fx-padding: 1em;");
		b.setOnAction(e -> programmeSearchDialogObjectProvider.getObject().showAndWait());
		getChildren().add(b);
	}
}
