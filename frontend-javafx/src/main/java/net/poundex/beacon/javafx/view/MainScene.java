package net.poundex.beacon.javafx.view;

import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.javafx.SoundPlayer;
import net.poundex.beacon.javafx.display.DisplayMode;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MainScene extends Scene {
	
	private final SoundPlayer soundPlayer;
	
	public MainScene(MainView mainView, Stage primaryStage, DisplayMode displayMode, SoundPlayer soundPlayer) {
		super(mainView);
		this.soundPlayer = soundPlayer;
		primaryStage.setScene(this);
		displayMode.acceptStage(primaryStage);
		getStylesheets().add(
				getClass().getClassLoader().getResource("beacon.css").toExternalForm());
		soundPlayer.configureForTouch(this);
		primaryStage.show();
	}
}
