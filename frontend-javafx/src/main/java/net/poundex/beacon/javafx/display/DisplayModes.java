package net.poundex.beacon.javafx.display;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum DisplayModes implements DisplayMode {

	DESKTOP(5, 3, 5, 5, 5, false, 1024, 600),
	RPI_7INCH_800_480(4, 2, 5, 5, 5, true, 0, 0),
	RPI_7INCH_1024_600(5, 3, 5, 5, 5, true, 0, 0);

	private final int cardCols;
	private final int cardRows;
	private final double cardHGap;
	private final double cardVGap;
	private final double cardPadding;
	private final boolean fullscreen;
	private final double width;
	private final double height;
}