package net.poundex.beacon.javafx.display;

import javafx.stage.Stage;

public interface DisplayMode {
	int getCardCols();
	int getCardRows();
	double getCardHGap();
	double getCardVGap();
	double getCardPadding();
	boolean isFullscreen();
	double getWidth();
	double getHeight();

	default void acceptStage(Stage stage) {
		if(isFullscreen())
			stage.setFullScreen(isFullscreen());
		else
			setSize(this, stage);
	}

	private static void setSize(DisplayMode displayMode, Stage stage) {
		stage.setWidth(displayMode.getWidth());
		stage.setHeight(displayMode.getHeight());
	}
}
