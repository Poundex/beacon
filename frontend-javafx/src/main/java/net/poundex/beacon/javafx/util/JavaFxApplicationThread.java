package net.poundex.beacon.javafx.util;

import javafx.application.Platform;
import reactor.core.Disposable;
import reactor.core.scheduler.Scheduler;

public enum JavaFxApplicationThread implements Scheduler {

	INSTANCE;

	@Override
	public Disposable schedule(Runnable task) {
		Platform.runLater(task);
		return this;
	}

	@Override
	public Worker createWorker() {
		return new Worker() {
			@Override
			public Disposable schedule(Runnable task) {
				JavaFxApplicationThread.this.schedule(task);
				return JavaFxApplicationThread.this;
			}

			@Override
			public void dispose() { }
		};
	}
}
