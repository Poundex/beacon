package net.poundex.beacon.javafx.util;

import javafx.scene.Node;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

import java.util.stream.IntStream;

public class SaneGridPane extends GridPane {
	@Override
	public void add(Node child, int columnIndex, int rowIndex) {
		add(child, columnIndex, rowIndex, 1, 1);
	}

	@Override
	public void add(Node child, int columnIndex, int rowIndex, int colspan, int rowspan) {
		super.add(child, columnIndex, rowIndex);

		if (child instanceof Region r)
			r.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		int colCount = getColumnCount();
		double pc = 100.0 / (double)colCount;
		getColumnConstraints().setAll(
				IntStream.range(0, colCount)
						.mapToObj(x -> new ColumnConstraints())
						.peek(x -> x.setPercentWidth(pc))
						.toList());

		setFillHeight(child, true);
		setFillWidth(child, true);
		setHgrow(child, Priority.ALWAYS);
		setVgrow(child, Priority.ALWAYS);
	}
}
