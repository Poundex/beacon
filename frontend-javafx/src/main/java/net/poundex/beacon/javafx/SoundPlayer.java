package net.poundex.beacon.javafx;

import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import net.poundex.beacon.javafx.view.MainScene;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.Queue;

@Service
public class SoundPlayer {
	
	private static final AudioClip touch =
			new AudioClip(MainScene.class.getResource("/sound/touch.wav").toExternalForm());
	
	private boolean touchWaiting = false;
	private Queue<String> soundQueue = new LinkedList<>();
	
	@PostConstruct
	public void init() {
		Thread thread = new Thread(() -> {
			while (true) {
				if (touchWaiting)
					doPlayTouch();

				String toPlay;
				while ((toPlay = soundQueue.poll()) != null)
					doPlaySound(toPlay);
			}

		});
		thread.setDaemon(true);
		thread.start();
	}

	public void playTouch() {
		touchWaiting = true;
	}

	private void doPlayTouch() {
		touch.play();
		touchWaiting = false;
	}

	public void configureForTouch(Scene target) {
		target.addEventFilter(MouseEvent.MOUSE_PRESSED, e -> playTouch());
	}

	private void doPlaySound(String toPlay) {
		new MediaPlayer(new Media(MainScene.class.getResource(
				String.format("/sound/%s.wav", toPlay)).toExternalForm()))
				.play();
	}

	public void playSound(String name) {
		soundQueue.add(name);
	}
}
