package net.poundex.beacon.javafx.client;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.javafx.BeaconApplicationConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.client.WebSocketGraphQlClient;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class ClientConfiguration {

	private final BeaconApplicationConfig applicationConfig;

	@Bean
	public WebSocketGraphQlClient webSocketGraphQlClient() {
		WebSocketClient client = new ReactorNettyWebSocketClient();
		return WebSocketGraphQlClient.builder(applicationConfig.getApiEndpoint(), client).build();
	}
}


