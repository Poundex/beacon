package net.poundex.beacon.javafx.view;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.tools.FlowGridPane;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import net.poundex.beacon.javafx.CardHandler;
import net.poundex.beacon.javafx.client.BeaconClient;
import net.poundex.beacon.javafx.display.DisplayMode;
import net.poundex.beacon.javafx.view.card.BasicCard;
import net.poundex.beacon.javafx.view.card.CardDialog;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Component;
import reactor.core.scheduler.Schedulers;

import java.util.HashMap;
import java.util.Map;

@Component
public class CardView extends FlowGridPane implements CardHandler {
	
	private final Map<String, BasicCard> cards = new HashMap<>();
	private final ObjectProvider<CardDialog> cardDialogObjectProvider;
	private final BeaconClient beaconClient;

	public CardView(DisplayMode displayMode, ObjectProvider<CardDialog> cardDialogObjectProvider, BeaconClient beaconClient) {
		super(displayMode.getCardCols(), displayMode.getCardRows());
		this.cardDialogObjectProvider = cardDialogObjectProvider;
		this.beaconClient = beaconClient;

		setBackground(new Background(new BackgroundFill(Tile.BACKGROUND.darker(), CornerRadii.EMPTY, Insets.EMPTY)));

		setHgap(displayMode.getCardHGap());
		setVgap(displayMode.getCardVGap());
		setPadding(new Insets(displayMode.getCardPadding()));
	}

	@Override
	public void showCard(String id, String text) {
		if(cards.containsKey(id))
			dismissCard(id);
		
		BasicCard card = new BasicCard(text);
		card.setOnMouseClicked(e -> showCardDialog(id, card));
		cards.put(id, card);
		doAddCard(card);
	}

	private void doAddCard(BasicCard card) {
		Platform.runLater(() -> getChildren().add(card));
	}

	private void dismissCard(String id) {
		beaconClient.dismissCard(id).subscribeOn(Schedulers.single()).subscribe();
	}

	@Override
	public void removeCard(String id) {
		BasicCard card = cards.remove(id);
		if(card != null)
			Platform.runLater(() -> getChildren().remove(card));
	}

	private void showCardDialog(String id, BasicCard card) {
		cardDialogObjectProvider.getObject()
				.setCard(card.copy())
				.showAndWait()
				.filter(bt -> bt == ButtonType.FINISH)
				.ifPresent(ignored -> dismissCard(id));
	}
}
