package net.poundex.beacon.javafx.view.card;

import javafx.scene.control.ButtonType;
import javafx.stage.StageStyle;
import jfxtras.styles.jmetro.FlatDialog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.javafx.SoundPlayer;
import net.poundex.beacon.javafx.util.JMetroUtil;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Lazy
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RequiredArgsConstructor
@Slf4j
public class CardDialog extends FlatDialog<ButtonType> {
	private final CardDialogPane cardDialogPane;
	private final SoundPlayer soundPlayer;
	
	@PostConstruct
	public void init() {
		initStyle(StageStyle.UNDECORATED);
		getDialogPane().setContent(cardDialogPane);
		getDialogPane().getButtonTypes().addAll(ButtonType.FINISH, ButtonType.CLOSE);
//		getDialogPane().setMaxHeight(500);
		JMetroUtil.INSTANCE.applyTheme(getDialogPane());
		setIconless(true);
		soundPlayer.configureForTouch(this.getDialogPane().getScene());
	}

	public CardDialog setCard(BasicCard card) {
		cardDialogPane.setCard(card);
		return this;
	}
}
