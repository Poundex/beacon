package net.poundex.beacon.javafx.util;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class CustomListCell<T> implements Callback<ListView<T>, ListCell<T>> {
	
	public static <T> CustomListCell<T> fromFactory(Function<T, Node> factoryFn) {
		return new CustomListCell<>(factoryFn);
	}
	
	private final Supplier<ListCell<T>> listCellFactory;
	private final List<Consumer<ListCell<T>>> customisers = new LinkedList<>();

	private CustomListCell(Function<T, Node> factoryFn) {
		this.listCellFactory = () -> new ListCell<>() {
			@Override
			public void updateItem(T item, boolean empty) {
				super.updateItem(item, empty);
				setGraphic( ! empty ? factoryFn.apply(item) : null);
			}
		};
	}

	@Override
	public ListCell<T> call(ListView<T> listView) {
		ListCell<T> listCell = listCellFactory.get();
		customisers.forEach(s -> s.accept(listCell));
		return listCell;
	}
	
	public CustomListCell<T> doWithListCell(Consumer<ListCell<T>> consumer) {
		customisers.add(consumer);
		return this;
	}
}
