package net.poundex.beacon.javafx.view;

import javafx.scene.layout.BorderPane;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class MainView extends BorderPane {

	private final CardView cardView;
	private final SidePanel sidePanel;
	
	@PostConstruct
	public void init() {
		setCenter(cardView);
		setRight(sidePanel);
	}
}
