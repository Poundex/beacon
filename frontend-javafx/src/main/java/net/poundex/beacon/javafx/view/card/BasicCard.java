package net.poundex.beacon.javafx.view.card;

import eu.hansolo.tilesfx.Tile;
import javafx.geometry.Pos;

public class BasicCard extends Tile {

	private String userAgentStyleSheet;

	public BasicCard(String text) {
		super(SkinType.TEXT);
		setDescription(text);
		setDescriptionAlignment(Pos.CENTER);
	}

	@Override
	public String getUserAgentStylesheet() {
		if (null == userAgentStyleSheet)
			userAgentStyleSheet = Tile.class.getResource("tilesfx.css").toExternalForm();

		return userAgentStyleSheet;
	}

	public BasicCard copy() {
		return new BasicCard(getDescription());
	}
}
