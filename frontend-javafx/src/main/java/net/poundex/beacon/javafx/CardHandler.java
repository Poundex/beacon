package net.poundex.beacon.javafx;

public interface CardHandler {
	void showCard(String id, String text);

	void removeCard(String id);
}
