package net.poundex.beacon.javafx;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "beacon")
@Data
@NoArgsConstructor
public class BeaconApplicationConfig {
	private String apiEndpoint;
}
