package net.poundex.beacon.javafx.client;

import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.graphql.client.GraphQlClient;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class BeaconClient {
	private static final ParameterizedTypeReference<List<Programme>> LIST_OF_PROGRAMMES =
			new ParameterizedTypeReference<>() { };
	

	private final GraphQlClient graphQlClient;

	public record Programme(String code, String name, String description) { }
	public record ProgrammeInstance(String id, Instant started, Programme programme) { }
	public record ClientCommandArg(String key, String value) { }
	public record ClientCommand(String command, List<ClientCommandArg> args) { }

	public Flux<Programme> programmes() {
		return graphQlClient.document("""
query programmes {
	programmes {
		name
		code
		description
	}
}
""")
				.retrieve("programmes")
				.toEntity(LIST_OF_PROGRAMMES)
				.flatMapMany(Flux::fromIterable);
	}

	public Flux<Programme> programmeSearch(String q) {
		return graphQlClient.document("""
query programmeSearch($q: String) {
    programmeSearch(q: $q) {
        name
        code
        description
    }
}
""")
				.variable("q", q)
				.retrieve("programmeSearch")
				.toEntity(LIST_OF_PROGRAMMES)
				.flatMapMany(Flux::fromIterable);
	}

	public Mono<ProgrammeInstance> startProgramme(Programme item) {
		return graphQlClient.document("""
mutation startProgramme($code: String) {
	startProgramme(code: $code) {
		id
		programme {
			code
		}
	}
}
""")
				.variable("code", item.code())
				.retrieve("startProgramme")
				.toEntity(ProgrammeInstance.class);
	}
	
	public Flux<ClientCommand> clientCommands() {
		return graphQlClient.document("""
subscription clientCommands {
	clientCommands {
		command
		args {
			key
			value
		}
	}
}
""")
				.retrieveSubscription("clientCommands")
				.toEntity(ClientCommand.class);
	}
	
	public Mono<Map<String, Object>> dismissCard(String id) {
		return graphQlClient.document("""
mutation dismissCard($id: String) {
	dismissCard(id: $id) {
		id
	}
}
""")
				.variable("id", id)
				.retrieve("dismissCard")
				.toEntity(new ParameterizedTypeReference<Map<String, Object>>() {});
	}
}
