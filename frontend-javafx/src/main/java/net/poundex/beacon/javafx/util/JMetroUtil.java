package net.poundex.beacon.javafx.util;

import javafx.scene.Node;
import javafx.scene.Parent;
import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.JMetroStyleClass;
import jfxtras.styles.jmetro.Style;

public enum JMetroUtil {
	
	INSTANCE;
	
	public JMetroUtil applyTheme(Parent parent) {
		new JMetro(parent, Style.DARK);
		return this;
	}

	public JMetroUtil background(Node node) {
		node.getStyleClass().add(JMetroStyleClass.BACKGROUND);
		return this;
	}
}
