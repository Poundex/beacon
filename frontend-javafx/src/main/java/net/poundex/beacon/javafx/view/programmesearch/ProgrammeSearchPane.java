package net.poundex.beacon.javafx.view.programmesearch;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.javafx.client.BeaconClient;
import net.poundex.beacon.javafx.client.BeaconClient.Programme;
import net.poundex.beacon.javafx.util.CustomListCell;
import net.poundex.beacon.javafx.util.JMetroUtil;
import net.poundex.beacon.javafx.util.JavaFxApplicationThread;
import net.poundex.beacon.javafx.util.SaneGridPane;
import reactor.core.publisher.Sinks;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
public class ProgrammeSearchPane extends BorderPane {
	
	private final Sinks.Many<String> numbers = Sinks.many().unicast().onBackpressureBuffer();
	private final Sinks.Many<String> search = Sinks.many().unicast().onBackpressureBuffer();
	private final Sinks.Many<List<Programme>> searchResults = Sinks.many().unicast().onBackpressureBuffer();
	
	private final BeaconClient beaconClient;
	private final Runnable dialogCloser;
	
	public void init() {
		JMetroUtil.INSTANCE.applyTheme(this).background(this);
		setCenter(searchView());
		setRight(keypad());
		search.asFlux().subscribe(this::doSearch);
		setMinWidth(700);
		setMaxHeight(200);
	}

	private void doSearch(String s) {
		if(s.isEmpty())
			searchResults.tryEmitNext(Collections.emptyList());
		else
			beaconClient.programmeSearch(s)
					.collectList()
					.subscribe(searchResults::tryEmitNext);
	}

	private void chooseProgramme(BeaconClient.Programme item) {
		beaconClient.startProgramme(item)
				.subscribe(pi -> 
						log.info("{} is newly a started {}", pi.id(), pi.programme().code()));
		dialogCloser.run();
	}
	
	/* **** */
	
	private BorderPane keypad() {
		BorderPane keypad = new BorderPane();
		TextField textField = new TextField();
		textField.setVisible(false);
		numbers.asFlux().subscribe(n -> {
			if(n.startsWith("<"))
				textField.deletePreviousChar();
			else if (n.equals("X"))
				textField.clear();
			else
				textField.appendText(n);

			search.tryEmitNext(textField.getText());
		});
		Label entryView = new Label();
		entryView.textProperty().bind(textField.textProperty());
		entryView.setStyle("""
  -fx-border-width: 1px;
  -fx-border-style: solid;
  -fx-alignment: center;
  -fx-font-size: x-large;
  -fx-font-weight: bold;
  -fx-padding: 0.5em;
  """);
		entryView.setMaxWidth(Double.MAX_VALUE);
		entryView.setMaxHeight(Double.MAX_VALUE);
		keypad.setTop(entryView);

		GridPane gp = new SaneGridPane();
		gp.add(createKeypadButton("1", ""), 0, 0);
		gp.add(createKeypadButton("2", "ABC"), 1, 0);
		gp.add(createKeypadButton("3", "DEF"), 2, 0);

		gp.add(createKeypadButton("4", "GHI"), 0, 1);
		gp.add(createKeypadButton("5", "JKL"), 1, 1);
		gp.add(createKeypadButton("6", "MNO"), 2, 1);

		gp.add(createKeypadButton("7", "PQRS"), 0, 2);
		gp.add(createKeypadButton("8", "TUV"), 1, 2);
		gp.add(createKeypadButton("9", "WXYZ"), 2, 2);

		gp.add(createKeypadButton("X", ""), 0, 3);
		gp.add(createKeypadButton("0", ""), 1, 3);
		gp.add(createKeypadButton("<X", ""), 2, 3);
		gp.setGridLinesVisible(true);

		keypad.setCenter(gp);
		return keypad;
	}

	private Button createKeypadButton(String number, String letters) {
		Button b = new Button();
		BorderPane bp = new BorderPane();

		Label nl = new Label(number);
//		nl.getStyleClass().add("keypad-number-label");
		String opac = number.contains("X") ? "" : "-fx-opacity: 0.5";
		nl.setStyle("""
  -fx-font-weight: bold;
  -fx-alignment: center;
  -fx-font-size: xx-large;
  """ + opac);
		bp.setCenter(nl);

		Label ll = new Label(letters);
//		ll.getStyleClass().add("keypad-letters-label");
		ll.setStyle("""
  -fx-font-weight: bold;
  -fx-alignment: center;
  """);
		ll.setMaxWidth(Double.MAX_VALUE);
		bp.setBottom(ll);

		bp.setStyle("-fx-background-color: transparent;");
		b.setGraphic(bp);
		b.setFocusTraversable(false);
		b.setOnAction(e -> numbers.tryEmitNext(number));
		return b;
	}

	private ListView<Programme> searchView() {
		ListView<BeaconClient.Programme> listView = new ListView<>();

		searchResults.asFlux()
				.publishOn(JavaFxApplicationThread.INSTANCE)
				.doOnNext(ignored -> listView.getItems().clear())
				.flatMapIterable(p -> p)
				.subscribe(result -> listView.getItems().add(result));

		listView.setCellFactory(
				CustomListCell.fromFactory(this::searchViewCell)
						.doWithListCell(lc -> lc.setOnMouseClicked(e -> chooseProgramme(lc.getItem()))));

		return listView;
	}

	private Node searchViewCell(BeaconClient.Programme programme) {
		BorderPane borderPane = new BorderPane();
		Label cl = new Label(programme.code());
		Label nl = new Label(programme.name());
		Label dl = new Label(programme.description());
		cl.setStyle("""
  -fx-opacity: 0.5;
  -fx-padding: 0.25em;
  -fx-font-size: x-large;
  """);
		nl.setStyle("""
  -fx-padding: 0.25em;
  -fx-font-size: x-large;
  -fx-font-weight: bold;
  """);
		dl.setStyle("""
  -fx-padding: 0.25em;
  -fx-font-size: large;
  """);
		borderPane.setTop(new FlowPane(cl, nl));
		borderPane.setBottom(dl);
		return borderPane;
	}
}
