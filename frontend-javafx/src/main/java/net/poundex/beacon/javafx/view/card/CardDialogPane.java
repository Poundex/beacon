package net.poundex.beacon.javafx.view.card;

import eu.hansolo.tilesfx.Tile;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.javafx.util.JMetroUtil;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@RequiredArgsConstructor
@Slf4j
@Component
public class CardDialogPane extends BorderPane {
	
	@PostConstruct
	public void init() {
		JMetroUtil.INSTANCE.applyTheme(this).background(this);
	}

	public void setCard(BasicCard card) {
		Pane pane = new Pane(card);
		pane.setStyle("-fx-padding: 1em;");
//		card.setStyle("--fx-font-size: 14px;");
		card.setTextSize(Tile.TextSize.BIGGER);
		setCenter(card);
	}
}
