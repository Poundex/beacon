package net.poundex.beacon.javafx;

import javafx.application.Application;
import javafx.stage.Stage;
import net.poundex.beacon.javafx.display.DisplayModes;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BeaconApplication extends Application {
	@Override
	public void start(Stage primaryStage) {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

		ctx.getBeanFactory().registerSingleton("primaryStage", primaryStage);
		ctx.getBeanFactory().registerSingleton("displayMode", DisplayModes.DESKTOP);

		ctx.register(BeaconApplicationContext.class);
		ctx.refresh();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
