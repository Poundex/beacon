package net.poundex.beacon.javafx;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.beacon.javafx.client.BeaconClient;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientCommandService {
	
	private final BeaconClient beaconClient;
	private final CardHandler cardHandler;
	private final SoundPlayer soundPlayer;
	
	@PostConstruct
	public void init() {
		beaconClient.clientCommands().subscribe(this::handleCommand);
	}

	private void handleCommand(BeaconClient.ClientCommand clientCommand) {
		switch(clientCommand.command()) {
			case "beacon.client.card.show"   -> handleShowCardCommand(getArgs(clientCommand));
			case "beacon.client.card.remove" -> handleRemoveCardCommand(getArgs(clientCommand));
			case "beacon.client.sound.play"  -> handlePlaySoundCommand(getArgs(clientCommand));
			default -> log.warn("Unknown client command {}" , clientCommand.command());
		}
	}

	private void handleShowCardCommand(Map<String, String> args) {
		cardHandler.showCard(args.get("beacon.client.card.id"), args.get("beacon.client.card.text"));
	}

	private void handleRemoveCardCommand(Map<String, String> args) {
		cardHandler.removeCard(args.get("beacon.client.card.id"));
	}

	private void handlePlaySoundCommand(Map<String, String> args) {
		soundPlayer.playSound(args.get("beacon.client.sound.name"));
	}

	private static Map<String, String> getArgs(BeaconClient.ClientCommand clientCommand) {
		return clientCommand.args().stream()
				.collect(Collectors.toMap(
						BeaconClient.ClientCommandArg::key, 
						BeaconClient.ClientCommandArg::value));
	}
}
