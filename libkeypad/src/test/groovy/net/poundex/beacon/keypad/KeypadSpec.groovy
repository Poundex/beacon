package net.poundex.beacon.keypad

import spock.lang.Specification

class KeypadSpec extends Specification {
	void "Convert single letters"() {
		expect:
		Keypad.INSTANCE.toNumeric("a") == "2"
		Keypad.INSTANCE.toNumeric("b") == "2"
		Keypad.INSTANCE.toNumeric("c") == "2"
		Keypad.INSTANCE.toNumeric("d") == "3"
		Keypad.INSTANCE.toNumeric("e") == "3"
		Keypad.INSTANCE.toNumeric("f") == "3"
		Keypad.INSTANCE.toNumeric("g") == "4"
		Keypad.INSTANCE.toNumeric("h") == "4"
		Keypad.INSTANCE.toNumeric("i") == "4"
		Keypad.INSTANCE.toNumeric("j") == "5"
		Keypad.INSTANCE.toNumeric("k") == "5"
		Keypad.INSTANCE.toNumeric("l") == "5"
		Keypad.INSTANCE.toNumeric("m") == "6"
		Keypad.INSTANCE.toNumeric("n") == "6"
		Keypad.INSTANCE.toNumeric("o") == "6"
		Keypad.INSTANCE.toNumeric("p") == "7"
		Keypad.INSTANCE.toNumeric("q") == "7"
		Keypad.INSTANCE.toNumeric("r") == "7"
		Keypad.INSTANCE.toNumeric("s") == "7"
		Keypad.INSTANCE.toNumeric("t") == "8"
		Keypad.INSTANCE.toNumeric("u") == "8"
		Keypad.INSTANCE.toNumeric("v") == "8"
		Keypad.INSTANCE.toNumeric("w") == "9"
		Keypad.INSTANCE.toNumeric("x") == "9"
		Keypad.INSTANCE.toNumeric("y") == "9"
		Keypad.INSTANCE.toNumeric("z") == "9"
	}
	
	void "Convert single numbers"() {
		expect:
		Keypad.INSTANCE.toNumeric("0") == "0"
		Keypad.INSTANCE.toNumeric("1") == "1"
		Keypad.INSTANCE.toNumeric("2") == "2"
		Keypad.INSTANCE.toNumeric("3") == "3"
		Keypad.INSTANCE.toNumeric("4") == "4"
		Keypad.INSTANCE.toNumeric("5") == "5"
		Keypad.INSTANCE.toNumeric("6") == "6"
		Keypad.INSTANCE.toNumeric("7") == "7"
		Keypad.INSTANCE.toNumeric("8") == "8"
		Keypad.INSTANCE.toNumeric("9") == "9"
	}
	
	void "Convert words"() {
		expect:
		Keypad.INSTANCE.toNumeric("AADZZ123") == "22399123"
		
		and: "Ignore unsupported"
		Keypad.INSTANCE.toNumeric("A?0") == "20"
	}
}
