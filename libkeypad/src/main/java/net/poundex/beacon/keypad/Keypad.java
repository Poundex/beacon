package net.poundex.beacon.keypad;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Keypad {
	
	INSTANCE;
	
	private final Map<String, String> theMap = generateTheMap();

	public String toNumeric(String input) {
		return input.toUpperCase().chars()
				.mapToObj(c -> Character.toString((char)c))
				.map(this::toNumericSingle)
				.collect(Collectors.joining());
	}
	
	private String toNumericSingle(String input) {
		if(theMap.containsKey(input)) 
			return theMap.get(input);
		
		return "";
	}

	@SuppressWarnings("unchecked")
	private static Map<String, String> generateTheMap() {
		return Map.ofEntries(
				Stream.concat(Stream.of(
								Map.entry("A", "2"),
								Map.entry("B", "2"),
								Map.entry("C", "2"),
								
								Map.entry("D", "3"),
								Map.entry("E", "3"),
								Map.entry("F", "3"),
								
								Map.entry("G", "4"),
								Map.entry("H", "4"),
								Map.entry("I", "4"),
								
								Map.entry("J", "5"),
								Map.entry("K", "5"),
								Map.entry("L", "5"),
								
								Map.entry("M", "6"),
								Map.entry("N", "6"),
								Map.entry("O", "6"),
								
								Map.entry("P", "7"),
								Map.entry("Q", "7"),
								Map.entry("R", "7"),
								Map.entry("S", "7"),
								
								Map.entry("T", "8"),
								Map.entry("U", "8"),
								Map.entry("V", "8"),
								
								Map.entry("W", "9"),
								Map.entry("X", "9"),
								Map.entry("Y", "9"),
								Map.entry("Z", "9")),

						"0123456789".chars()
								.mapToObj(c -> Character.toString((char)c))
								.map(s -> Map.entry(s, s))).toArray(Map.Entry[]::new));
		
	}
}
